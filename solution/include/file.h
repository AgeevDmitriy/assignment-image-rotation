#ifndef FILE_H
#define FILE_H

#include "../include/rotation.h"
#include <stdbool.h>
#include <stdio.h>

enum status_code {
    OK = 0,
    F_OPEN_ERROR,
    F_R_ERROR,
    H_R_ERROR,
    C_R_ERROR,
    H_WR_ERROR,
    C_WR_ERROR,
    F_WR_ERROR,
    F_CLOSE_ERROR,
    F_FORMAT_ERROR
};

void print_status_code(enum status_code status_code);
bool f_open(const char* file_name, const char* mode, struct image* image);
void f_close(FILE* file);

#endif //FILE_H
