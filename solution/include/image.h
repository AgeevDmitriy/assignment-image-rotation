#ifndef IMAGE_H
#define IMAGE_H

#include <malloc.h>
#include <stdint.h>

struct image {
    uint64_t height;
    uint64_t width;
    struct pixel* pixel;
};

struct pixel{
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

struct image image_create(uint64_t height, uint64_t width);
void free_image(struct image image);
void set_pixel(struct image image, struct pixel pixel, uint64_t row, uint64_t column);
struct pixel get_pixel(struct image image, uint64_t row, uint64_t column);
#endif //IMAGE_H
