#include "../include/image.h"

struct image image_create(uint64_t height, uint64_t width) {
    struct image image = {0};
    image.height = height;
    image.width = width;
    image.pixel = malloc(sizeof(struct pixel) * image.width * image.height);
    return image;
}

void free_image(struct image image) {
    free(image.pixel);
}

void set_pixel(struct image image, const struct pixel pixel, uint64_t row, uint64_t column) {
    image.pixel[row * image.width + column] = pixel;
}


struct pixel get_pixel(const struct image image, uint64_t row, uint64_t column) {
    struct pixel res = image.pixel[row * image.width + column];
    return res;
}
