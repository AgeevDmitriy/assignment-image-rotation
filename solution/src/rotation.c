#include "../include/rotation.h"
#include "../include/image.h"

struct image rotating(struct image const source) {
    struct image rotating_image = image_create(source.width, source.height);

    for (size_t i = 0; i < source.width; i++) {
        for (size_t j = 0; j < source.height; j++) {
            struct pixel pixel = get_pixel(source, j, i);
            set_pixel(rotating_image, pixel, i, rotating_image.width - 1 - j);
        }
    }
    return rotating_image;
}
